import "assets/styles/App.scss";
import { RouterProvider } from "atomic-router-react";
import { RoutesView, router } from "config/routeConfig/routeConfig";

function App() {
  return (
    <RouterProvider router={router}>
      <RoutesView />
    </RouterProvider>
  );
}

export default App;
