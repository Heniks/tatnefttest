import { $finishStore } from "@/store/finishStore/store";
import { useStore } from "effector-react";
import { Question } from "@/components/Question/Question";
import styles from "./FinishPage.module.scss";
import { Button } from "@mui/material";
import {
  $questionsStore,
  restartButtonPressed,
} from "@/store/questionsStore/store";
import { $startPageStore } from "@/store/startPageStore/store";
import { clearLocalStorage } from "@/helpers/localStorage/localStorage";

const handleRestart = () => {
  clearLocalStorage();

  $startPageStore.reset();
  $questionsStore.reset();

  restartButtonPressed();
};

export default function FinishPage() {
  const { startPageStore, questionsStore } = useStore($finishStore);
  const { userAnswers, questions: questionsData, count } = questionsStore;

  return (
    <div>
      <h1>Поздравляю, {startPageStore.name}</h1>
      <h3 className={styles.historyTitle}>Ваши баллы: {count} </h3>
      <h3 className={styles.historyTitle}>История ответов:</h3>
      <ul className={styles.list}>
        {questionsData.map((question, index) => (
          <li key={question.id}>
            <Question
              type="answer"
              questionData={question}
              index={index}
              userAnswer={userAnswers[index]}
            />
          </li>
        ))}
      </ul>
      <Button variant="contained" onClick={handleRestart}>
        Начать сначала
      </Button>
    </div>
  );
}
