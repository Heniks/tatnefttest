import { Question } from "@/components/Question/Question";
import styles from "./TestPage.module.scss";
import { Button } from "@mui/material";
import { continueButtonPressed } from "@/store/questionsStore/store";
import { IQuestionObj } from "./TestPage.interface";
import cn from "classnames";
import { buttonStyles } from "@/helpers/Mui/sx";
import { Progress } from "@/components/Progress/Progress";
import { useTestPage } from "@/hooks/useTestPage/useTestPage";

const handleOnClick = (type: IQuestionObj["type"]) => {
  continueButtonPressed(type);
};

export default function TestPage() {
  const { currentQuestion, currentQuestionIndex, error } = useTestPage();
  return (
    <div className={styles.wrapper}>
      <Progress />
      <h3>Вопрос №: {currentQuestionIndex + 1}</h3>
      <span> Выберите один из вариантов ответа:</span>
      <div className={styles.questionWrapper}>
        <Question
          type="question"
          questionData={currentQuestion}
          index={currentQuestionIndex}
        />
      </div>
      <div
        className={cn("error", {
          show: error,
        })}
      >
        {error}
      </div>
      <Button
        sx={buttonStyles}
        variant="contained"
        onClick={() => handleOnClick(currentQuestion.type)}
      >
        Продолжить
      </Button>
    </div>
  );
}
