export interface IQuestionObj {
  id: number;
  label: string;
  type: string;
  answers: { value: string; label: string }[];
  correct: string;
}
