import { lazy } from "react";

export const StartPageLazy = lazy(() => import("./StartPage"));
// export const StartPageLazy = lazy(
//   () =>
//     new Promise((res) => {
//       setTimeout(() => {
//         res("foo");
//       }, 30000);
//     })
// );
