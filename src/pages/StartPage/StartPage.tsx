import { useStore } from "effector-react";
import {
  setName,
  startButtonClicked,
  $error,
} from "@/store/startPageStore/store";
import { Input } from "@/components/Input/Input";
import { Button } from "@mui/material";
import styles from "./StartPage.module.scss";

const handleName = (e: React.ChangeEvent<HTMLInputElement>) =>
  setName(e.target.value);

const startHandler = () => {
  startButtonClicked();
};

export default function StartPage() {
  const error = useStore($error);

  return (
    <div className={styles.wrapper}>
      <div className={styles.top}>
        <h1>Добро пожаловать</h1>
        <h3>JavaScript тестирование</h3>
      </div>
      <form className={styles.form}>
        <Input
          onChange={handleName}
          placeholder="Имя"
          errorText={error}
          autoFocus
        />
        <Button
          className={styles.btn}
          variant="contained"
          type="button"
          onClick={startHandler}
        >
          Начать!
        </Button>
      </form>
    </div>
  );
}
