import { setLocalStorageItem } from "@/helpers/localStorage/localStorage";

export interface IUseLocalStorage {
  value: any;
  setLocalStorageItem?: typeof setLocalStorageItem;
  getLocalStorageItem?: typeof setLocalStorageItem;
}
