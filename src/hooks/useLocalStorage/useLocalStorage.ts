import {
  getLocalStorageItem,
  setLocalStorageItem,
} from "@/helpers/localStorage/localStorage";
import { IUseLocalStorage } from "./useLocalStorage.interface";

export function useLocalStorage(key: LocalStorageKeysType): IUseLocalStorage {
  if (typeof getLocalStorageItem(key) !== undefined)
    return {
      value: getLocalStorageItem(key),
      getLocalStorageItem,
      setLocalStorageItem,
    };

  return {
    value: null,
  };
}
