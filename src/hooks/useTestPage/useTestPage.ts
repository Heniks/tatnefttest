import { $questionsStore, $testError } from "@/store/questionsStore/store";
import { useStore } from "effector-react";
import { IUseTestPageObj } from "./userTestPage.interface";

export const useTestPage = (): IUseTestPageObj => {
  //доступ к данным
  const { currentQuestionIndex, questions: questionData } =
    useStore($questionsStore);
  const error = useStore($testError);

  const currentQuestion = questionData[currentQuestionIndex]; //текущий объект вопроса

  return {
    currentQuestionIndex,
    error,
    currentQuestion,
  };
};
