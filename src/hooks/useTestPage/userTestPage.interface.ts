import { IQuestionObj } from "@/pages/TestPage/TestPage.interface";

export interface IUseTestPageObj {
  currentQuestionIndex: number;
  error: string;
  currentQuestion: IQuestionObj;
}
