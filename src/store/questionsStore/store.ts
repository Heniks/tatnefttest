import { createEffect, createStore, sample, restore } from "effector";
import { IQuestionStore } from "./store.interface";
import { createEvent } from "effector/compat";
import questions from "../../../questions.json";
import { IQuestionObj } from "@/pages/TestPage/TestPage.interface";
import { redirect } from "atomic-router";
import { finishRoute } from "@/config/routeConfig/routeConfig";
import {
  clearLocalStorage,
  countKey,
  currentActiveIndexKey,
  getLocalStorageItem,
  setLocalStorageItem,
  userAnswersKey,
} from "@/helpers/localStorage/localStorage";
import { startButtonClicked } from "../startPageStore/store";

export const toNextQuestion = createEvent();
export const answerSelected = createEvent<{
  questionId: number;
  answer: string;
}>();
export const continueButtonPressed = createEvent<IQuestionObj["type"]>();
export const restartButtonPressed = createEvent();

const toFinishFx = createEffect<IQuestionStore, null, string>(
  ({ toFinish }) => {
    if (!toFinish) throw "Не  конец";
    return null;
  }
);

const validateFx = createEffect<IQuestionStore, null, string>(
  ({ userAnswers, currentQuestionIndex }) => {
    const isEmpty = typeof userAnswers[currentQuestionIndex] === "undefined";
    if (isEmpty) throw "Выберите ответ";

    return null;
  }
);

export const $testError = restore(validateFx.failData, "").reset(
  answerSelected
);

export const $questionsStore = createStore<IQuestionStore>({
  currentQuestionIndex: getLocalStorageItem(currentActiveIndexKey) || 0,
  questions,
  count: getLocalStorageItem(countKey) || 0,
  lastQuestionIndex: questions.length,
  toFinish: false,
  userAnswers: getLocalStorageItem(userAnswersKey) || [],
})
  .on(toNextQuestion, (store) => {
    const { currentQuestionIndex, lastQuestionIndex } = store;

    const isNextLast = currentQuestionIndex + 1 === lastQuestionIndex;
    //если след вопрос не последний
    const nextIndex = isNextLast
      ? currentQuestionIndex
      : currentQuestionIndex + 1;

    setLocalStorageItem(currentActiveIndexKey, nextIndex);

    return {
      ...store,
      toFinish: isNextLast,
      currentQuestionIndex: nextIndex,
    };
  })
  .on(answerSelected, (store, { questionId, answer = "" }) => {
    const existingAnswerIndex = store.userAnswers.findIndex(
      (userAnswer) => userAnswer.questionId === questionId
    );

    const correctAnswer = store.questions[store.currentQuestionIndex].correct;

    if (existingAnswerIndex !== -1) {
      // Если ответ для этого вопроса уже существует, обновить его
      const updatedUserAnswers = store.userAnswers.slice();
      updatedUserAnswers[existingAnswerIndex] = {
        questionId,
        answer,
        correctAnswer,
      };
      setLocalStorageItem(userAnswersKey, updatedUserAnswers);

      return {
        ...store,
        userAnswers: updatedUserAnswers,
      };
    } else {
      // В противном случае, добавить новый ответ
      const updatedUserAnswers = [
        ...store.userAnswers,
        { questionId, answer: answer, correctAnswer },
      ];
      setLocalStorageItem(userAnswersKey, updatedUserAnswers);
      return {
        ...store,
        userAnswers: updatedUserAnswers,
      };
    }
  })
  .on(continueButtonPressed, (store) => {
    const { currentQuestionIndex, userAnswers, count } = store;
    const currentQuestion = userAnswers[currentQuestionIndex];
    const isCorrectAnswer =
      currentQuestion?.answer === currentQuestion?.correctAnswer;
    const currentCount = isCorrectAnswer ? count + 1 : count;

    setLocalStorageItem(countKey, currentCount);

    return { ...store, count: currentCount };
  })
  .reset(restartButtonPressed)
  .on(startButtonClicked, (store) => {
    clearLocalStorage();
    return { ...store, currentQuestionIndex: 0, userAnswers: [], count: 0 };
  });

//запускаем валидацию
sample({
  clock: continueButtonPressed,
  source: $questionsStore,
  target: validateFx,
});

//если валидация прошла, идем к следующему вопросу
sample({
  clock: validateFx.done,
  source: $questionsStore,
  target: toNextQuestion,
});

//смотрим не последний ли следующий вопрос
sample({
  clock: toNextQuestion,
  source: $questionsStore,
  target: toFinishFx,
});

//eсли следующий вопрос последний, то к редирект на финиш
redirect({
  clock: toFinishFx.done,
  route: finishRoute,
});
