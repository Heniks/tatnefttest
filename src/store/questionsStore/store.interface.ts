import { IQuestionObj } from "@/pages/TestPage/TestPage.interface";

export interface IQuestionStore {
  currentQuestionIndex: number;
  lastQuestionIndex: number;
  toFinish: boolean;
  count: number;
  questions: IQuestionObj[];
  userAnswers: { answer: string; questionId: number; correctAnswer: string }[];
}
