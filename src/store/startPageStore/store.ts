import { createStore, sample, restore } from "effector";
import { IStartPageStore } from "./store.interface";
import { createEvent } from "effector/effector.mjs";
import { createEffect } from "effector/effector.umd";
import { redirect } from "atomic-router";
import { testRoute } from "@/config/routeConfig/routeConfig";
import {
  getLocalStorageItem,
  nameKey,
  setLocalStorageItem,
} from "@/helpers/localStorage/localStorage";

export const setName = createEvent<string>();
export const validateName = createEvent<void>();
export const addQuestionCount = createEvent<void>();
export const startButtonClicked = createEvent();

const validateFx = createEffect<IStartPageStore, null, string>((store) => {
  const isEmpty = !store.name.trim().length;
  if (isEmpty) throw "Обязательное поле";

  return null;
});

export const $error = restore(validateFx.failData, "").reset(setName);

export const $startPageStore = createStore<IStartPageStore>({
  name: getLocalStorageItem(nameKey) || "",
}).on(setName, (store, name) => {
  setLocalStorageItem(nameKey, name);
  return { ...store, name };
});

//запускаем валидацию
sample({
  clock: startButtonClicked,
  source: $startPageStore,
  target: validateFx,
});

// если валидация прошла, переходим на страницу тестирования
redirect({
  clock: validateFx.done,
  route: testRoute,
});
