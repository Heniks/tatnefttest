import { combine, createEffect, sample } from "effector";
import { IFinishStore } from "./store.interface";
import { $startPageStore } from "../startPageStore/store";
import { $questionsStore, restartButtonPressed } from "../questionsStore/store";
import { startRoute } from "@/config/routeConfig/routeConfig";
import { redirect } from "atomic-router";
import { clearLocalStorage } from "@/helpers/localStorage/localStorage";

export const clearLocalStorageFx = createEffect(() => {
  clearLocalStorage();
  $questionsStore.reset();
  $startPageStore.reset();
  $finishStore.reset();
});

export const $finishStore = combine<IFinishStore>({
  startPageStore: $startPageStore,
  questionsStore: $questionsStore,
});

//чистим localstorage если нажали рестарт
sample({
  clock: restartButtonPressed,
  target: clearLocalStorageFx,
});

// нажата кнопка начать сначала -> to start page
redirect({
  clock: clearLocalStorageFx.done,
  route: startRoute,
});
