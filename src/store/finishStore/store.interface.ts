import { IQuestionStore } from "../questionsStore/store.interface";
import { IStartPageStore } from "../startPageStore/store.interface";

export interface IFinishStore {
  startPageStore: IStartPageStore;
  questionsStore: IQuestionStore;
}
