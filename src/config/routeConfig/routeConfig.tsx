import { MainLayout } from "@/components/Layouts/MainLayout/MainLayout";
import { FinsihPageLazy } from "@/pages/FinishPage/FinishPagee.lazy";
import { StartPageLazy } from "@/pages/StartPage/StartPage.lazy";
import { TestPageLazy } from "@/pages/TestPage/TestPage.lazy";
import {
  createHistoryRouter,
  createRoute,
  createRouterControls,
} from "atomic-router";
import { createRoutesView } from "atomic-router-react";
import { createBrowserHistory } from "history";

export const startRoute = createRoute();
export const testRoute = createRoute();
export const finishRoute = createRoute();

const routes = [
  { path: "/", route: startRoute },
  { path: "/test", route: testRoute },
  { path: "/finish", route: finishRoute },
];

export const router = createHistoryRouter({
  routes: routes,
});

const routesView = [
  { route: startRoute, view: StartPageLazy, layout: MainLayout },
  { route: testRoute, view: TestPageLazy, layout: MainLayout },
  { route: finishRoute, view: FinsihPageLazy, layout: MainLayout },
];

export const history = createBrowserHistory();
export const controls = createRouterControls();

router.setHistory(history);

export const RoutesView = createRoutesView({
  routes: routesView,
  otherwise() {
    return <div>Page not found!</div>;
  },
});
