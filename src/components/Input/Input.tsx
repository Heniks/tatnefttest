import { FormLabel, Input as MuiInput, inputClasses } from "@mui/material";
import { IInputProps } from "./Input.interface";
import styles from "./Input.module.scss";
import cn from "classnames";

export function Input({ onChange, id, errorText, ...inputProps }: IInputProps) {
  return (
    <FormLabel htmlFor={id}>
      Ваше имя:
      <MuiInput
        sx={{
          [`&.${inputClasses.root}`]: {
            marginLeft: "8px",
          },
        }}
        className={styles.input}
        onChange={onChange}
        id={id}
        {...inputProps}
      />
      <div
        className={cn("error", {
          show: errorText,
        })}
      >
        {errorText}
      </div>
    </FormLabel>
  );
}
