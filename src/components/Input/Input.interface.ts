import { InputProps } from "@mui/material";

export interface IInputProps extends InputProps {
  errorText?: string;
}
