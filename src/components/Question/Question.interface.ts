import { IQuestionObj } from "@/pages/TestPage/TestPage.interface";

export interface IQuestion {
  questionData: IQuestionObj;
  index: number;
  type: string;
  userAnswer?: { questionId: number; answer: string; correctAnswer: string };
}
