import {
  FormControl,
  FormLabel,
  RadioGroup,
  FormControlLabel,
} from "@mui/material";

import { getControlElement } from "@/helpers/getControlElement/getControlElement";
import { IQuestion } from "./Question.interface";
import { answerSelected } from "@/store/questionsStore/store";
import { ControlElementTypes } from "@/helpers/getControlElement/getControlElement.interface";
import { formControlLabelSx } from "@/helpers/Mui/formControlLabelSx";
import { formLabelSx } from "@/helpers/Mui/sx";

const onChange = (checked: boolean, id: number, value: string) => {
  if (checked) answerSelected({ questionId: id, answer: value });
};

export const Question: React.FC<IQuestion> = ({
  questionData,
  index,
  type,
  userAnswer,
}) => {
  const isAnswer = type === "answer";

  return (
    <div>
      <FormControl key={questionData.id}>
        <FormLabel sx={formLabelSx}>
          {index + 1}) {questionData.label}
        </FormLabel>
        <RadioGroup
          name="radio-buttons-group"
          onChange={(e, value) =>
            onChange(e.target.checked, questionData.id, value)
          }
        >
          {questionData.answers.map((answer) => {
            //ответ пользователя
            const usrAnswer = answer.value === userAnswer?.answer;
            //если finish страница, то покажи вариант ответа пользователя
            const checked = isAnswer ? usrAnswer : undefined;
            // ответил ли пользователь правильно
            const isCorrectAnswer =
              userAnswer?.answer === userAnswer?.correctAnswer;

            return (
              <FormControlLabel
                sx={formControlLabelSx(checked, !isCorrectAnswer)}
                checked={checked}
                disabled={isAnswer}
                key={answer.label}
                value={answer.value}
                control={getControlElement(
                  questionData.type as ControlElementTypes
                )}
                label={answer.label}
              />
            );
          })}
        </RadioGroup>
      </FormControl>
    </div>
  );
};
