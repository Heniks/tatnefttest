import { progresSx } from "@/helpers/Mui/sx";
import LinearProgress from "@mui/material/LinearProgress";

import { useStore } from "effector-react";
import { $questionsStore } from "@/store/questionsStore/store";

export const Progress: React.FC = () => {
  const { currentQuestionIndex, lastQuestionIndex } = useStore($questionsStore);
  const percent = (currentQuestionIndex * 100) / (lastQuestionIndex - 1);

  return (
    <LinearProgress
      sx={progresSx(percent)}
      variant="determinate"
      value={percent}
    />
  );
};
