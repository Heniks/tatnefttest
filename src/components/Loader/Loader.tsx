import { CircularProgress, circularProgressClasses } from "@mui/material";
import styles from "./Loader.module.scss";

export const Loader = () => {
  return (
    <div className={styles.loader}>
      <CircularProgress
        size={75}
        sx={{
          [`&.${circularProgressClasses.root}`]: {
            color: "yellow",
          },
        }}
      />
    </div>
  );
};
