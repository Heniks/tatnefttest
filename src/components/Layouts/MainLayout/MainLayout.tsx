import styles from "./MainLayout.module.scss";
import { Container } from "components/Container/Container";
import { Suspense } from "react";
import { Loader } from "components/Loader/Loader";

export function MainLayout({ children }: { children: React.ReactNode }) {
  return (
    <Container>
      <main className={styles.main}>
        <section className={styles.content}>
          <Suspense fallback={<Loader />}>{children}</Suspense>
        </section>
      </main>
    </Container>
  );
}
