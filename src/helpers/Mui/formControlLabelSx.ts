import { formControlLabelClasses } from "@mui/material/FormControlLabel";

export const getBackgroundColor = (
  checked: boolean | undefined,
  uncorrectAnswer: boolean
) => {
  if (checked === undefined) {
    return "none";
  } else if (checked) {
    return uncorrectAnswer ? "red" : "lawngreen";
  } else {
    return "none";
  }
};

export const formControlLabelSx = (
  checked: boolean | undefined,
  uncorrectAnswer: boolean
) => {
  const background = getBackgroundColor(checked, uncorrectAnswer);

  return {
    [`&.${formControlLabelClasses.root}`]: {
      background: background,
      borderRadius: "8px",
    },
    "&.MuiFormControlLabel-root .MuiFormControlLabel-label.Mui-disabled": {
      color: "#000",
    },
  };
};
