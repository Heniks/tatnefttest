export const getColorFromPercent = (percent: number): string => {
  if (percent <= 50) return "red";
  if (percent <= 76) return "yellow";

  return "lawngreen";
};
