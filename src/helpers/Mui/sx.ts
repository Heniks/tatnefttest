import { buttonClasses } from "@mui/material/Button";
import { formLabelClasses } from "@mui/material/FormLabel";
import { getColorFromPercent } from "./getColorFromPercent";

export const buttonStyles = {
  [`&.${buttonClasses.root}`]: {
    marginTop: "16px",
    flex: 0,
  },
};

export const formLabelSx = {
  [`&.${formLabelClasses.root}`]: { marginBottom: "8px" },
};

export const progresSx = (percent: number) => {
  return {
    marginBottom: "16px",
    "& .MuiLinearProgress-bar": {
      backgroundColor: getColorFromPercent(percent),
    },
  };
};
