type LocalStorageKeysType =
  | "currentActiveIndex"
  | "userAnswers"
  | "name"
  | "count";
