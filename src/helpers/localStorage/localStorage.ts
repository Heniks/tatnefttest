export const currentActiveIndexKey = "currentActiveIndex";
export const userAnswersKey = "userAnswers";
export const nameKey = "name";
export const countKey = "count";

export const setLocalStorageItem = (key: LocalStorageKeysType, value: any) =>
  localStorage.setItem(key, JSON.stringify(value));

export const getLocalStorageItem = (key: LocalStorageKeysType) => {
  let value: string | null = "";
  try {
    value = localStorage.getItem(key);
    return value ? JSON.parse(value) : null;
  } catch (error) {
    console.log(error);
  }
};

export const clearLocalStorage = () => localStorage.clear();
