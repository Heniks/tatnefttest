export type ControlElementTypes = "default" | "radio" | "checkbox";

export type GetControlElementType = {
  [key in ControlElementTypes]: React.ReactElement;
};
