import { Checkbox, Radio } from "@mui/material";
import {
  ControlElementTypes,
  GetControlElementType,
} from "./getControlElement.interface";

const controls: GetControlElementType = {
  default: <Radio />,
  radio: <Radio />,
  checkbox: <Checkbox />,
};

export function getControlElement(type: ControlElementTypes) {
  return controls[type || "default"];
}
